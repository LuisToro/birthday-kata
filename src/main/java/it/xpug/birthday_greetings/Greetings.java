package it.xpug.birthday_greetings;

public class Greetings {
	private String recipient;
	private String body;
	private String subject;
	
	public Greetings(Employee employee) {
		recipient = employee.getEmail();
		body = "Happy Birthday, dear %NAME%".replace("%NAME%", employee.getFirstName());
		subject = "Happy Birthday!";
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}
