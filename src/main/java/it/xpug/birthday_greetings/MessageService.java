package it.xpug.birthday_greetings;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MessageService implements IMessageService{
	private String host;
	private int port;
	
	MessageService(String host, int port) {
		this.setHost(host);
		this.setPort(port);
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void send(Greetings greetings) throws AddressException, MessagingException{
		// Create a mail session
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "" + port);
		Session session = Session.getInstance(props, null);
		
		// Construct the message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("ucb"));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(greetings.getRecipient()));
		msg.setSubject(greetings.getSubject());
		msg.setText(greetings.getBody());

		// Send the message
		Transport.send(msg);
	}

}
