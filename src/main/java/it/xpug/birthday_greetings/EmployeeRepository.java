package it.xpug.birthday_greetings;

public class EmployeeRepository implements IEmployeeRepository{
private String file;
	
	public EmployeeRepository(String file) {
		this.setFile(file);
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
